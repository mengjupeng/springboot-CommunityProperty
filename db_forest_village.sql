/*
 Navicat Premium Data Transfer

 Source Server         : wanghuan
 Source Server Type    : MySQL
 Source Server Version : 80012
 Source Host           : localhost:3306
 Source Schema         : db_forest_village

 Target Server Type    : MySQL
 Target Server Version : 80012
 File Encoding         : 65001

 Date: 11/11/2020 19:55:54
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for forest_cararea
-- ----------------------------
DROP TABLE IF EXISTS `forest_cararea`;
CREATE TABLE `forest_cararea`  (
  `carAreaID` int(11) NOT NULL AUTO_INCREMENT COMMENT '车辆关联编号',
  `carID` int(11) NULL DEFAULT NULL COMMENT '车位编号',
  `yeZhuID` int(11) NULL DEFAULT NULL COMMENT '业主编号',
  `typeID` int(11) NULL DEFAULT NULL COMMENT '车位编号',
  `areaTypeID` int(11) NULL DEFAULT NULL COMMENT '车位区域编号',
  `carDate` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '车位时间',
  `recordStates` int(11) NULL DEFAULT NULL COMMENT '车辆是否存在状态',
  PRIMARY KEY (`carAreaID`) USING BTREE,
  INDEX `carID`(`carID`) USING BTREE,
  INDEX `typeID`(`typeID`) USING BTREE,
  INDEX `yeZhuID`(`yeZhuID`) USING BTREE,
  INDEX `areaTypeID`(`areaTypeID`) USING BTREE,
  CONSTRAINT `areaTypeID` FOREIGN KEY (`areaTypeID`) REFERENCES `forest_carareatype` (`carareatypeid`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `carID` FOREIGN KEY (`carID`) REFERENCES `forest_carmessage` (`carid`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `typeID` FOREIGN KEY (`typeID`) REFERENCES `forest_cartype` (`typeID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `yeZhuID` FOREIGN KEY (`yeZhuID`) REFERENCES `forest_yezhumessage` (`yezhuid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of forest_cararea
-- ----------------------------
INSERT INTO `forest_cararea` VALUES (1, 1, 1, 1, 1, '2020-10-24 16:35:15', 1);
INSERT INTO `forest_cararea` VALUES (3, 4, 8, 4, 2, '2020-11-01 10:31:43', 1);
INSERT INTO `forest_cararea` VALUES (4, 5, 6, 6, 1, '2020-11-01 11:06:35', 2);
INSERT INTO `forest_cararea` VALUES (5, 6, 7, 3, 2, '2020-11-01 11:07:03', 1);
INSERT INTO `forest_cararea` VALUES (6, 7, 10, 4, 1, '2020-11-01 11:07:34', 1);
INSERT INTO `forest_cararea` VALUES (7, 8, 39, 2, 2, '2020-11-01 11:08:08', 1);

-- ----------------------------
-- Table structure for forest_carareatype
-- ----------------------------
DROP TABLE IF EXISTS `forest_carareatype`;
CREATE TABLE `forest_carareatype`  (
  `carAreaTypeID` int(11) NOT NULL AUTO_INCREMENT COMMENT '车位区域编号',
  `carAreaTypeName` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '车位区域名称',
  `carAreaTypeDate` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '车位区域时间',
  `carAreaTypeBeiZhu` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '车位区域备注',
  PRIMARY KEY (`carAreaTypeID`) USING BTREE,
  INDEX `carAreaTypeID`(`carAreaTypeID`) USING BTREE,
  INDEX `carAreaTypeID_2`(`carAreaTypeID`) USING BTREE,
  INDEX `carAreaTypeID_3`(`carAreaTypeID`) USING BTREE,
  INDEX `carAreaTypeID_4`(`carAreaTypeID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of forest_carareatype
-- ----------------------------
INSERT INTO `forest_carareatype` VALUES (1, 'A区', '2020-10-24 16:35:15', NULL);
INSERT INTO `forest_carareatype` VALUES (2, 'B区', '2020-10-24 16:35:16', NULL);

-- ----------------------------
-- Table structure for forest_carmessage
-- ----------------------------
DROP TABLE IF EXISTS `forest_carmessage`;
CREATE TABLE `forest_carmessage`  (
  `carID` int(11) NOT NULL AUTO_INCREMENT COMMENT '车位ID',
  `carImage` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '车辆照片',
  `carPlates` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '车牌照',
  `carStates` int(255) NULL DEFAULT NULL COMMENT '车位状态(\r\n2 车位未售出\r\n1  车位已售出)',
  `catDate` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '车位时间',
  `carBeiZhu` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`carID`) USING BTREE,
  INDEX `yeZhuID_id`(`catDate`) USING BTREE,
  INDEX `carID`(`carID`) USING BTREE,
  INDEX `carID_2`(`carID`) USING BTREE,
  INDEX `carID_3`(`carID`) USING BTREE,
  INDEX `carID_4`(`carID`) USING BTREE,
  INDEX `carID_5`(`carID`) USING BTREE,
  INDEX `carID_6`(`carID`) USING BTREE,
  INDEX `carID_7`(`carID`) USING BTREE,
  INDEX `carID_8`(`carID`) USING BTREE,
  INDEX `carID_9`(`carID`) USING BTREE,
  INDEX `carID_10`(`carID`) USING BTREE,
  INDEX `carID_11`(`carID`) USING BTREE,
  INDEX `carID_12`(`carID`) USING BTREE,
  INDEX `carID_13`(`carID`) USING BTREE,
  INDEX `carID_14`(`carID`) USING BTREE,
  INDEX `carID_15`(`carID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of forest_carmessage
-- ----------------------------
INSERT INTO `forest_carmessage` VALUES (1, '1604318347020d16e1d21a9791f16576ee999d8950d41.jpg', '豫666966', 1, '2020-10-25 16:25:04', NULL);
INSERT INTO `forest_carmessage` VALUES (4, '16043179174250ac8c6235fbb290dcb7f05cb722a6763.jpg', '豫5626256', 1, '2020-11-01 10:31:43', NULL);
INSERT INTO `forest_carmessage` VALUES (5, '1604317910775d16e1d21a9791f16576ee999d8950d41.jpg', '鲁234432', 1, '2020-11-01 11:06:35', NULL);
INSERT INTO `forest_carmessage` VALUES (6, '160431790475277dd2aa68707deeae199abadf8877317.jpg', '鲁2456545', 1, '2020-11-01 11:07:03', NULL);
INSERT INTO `forest_carmessage` VALUES (7, '16043178934000c889b0474120ee6596465ec392daba2.jpg', '豫2456545', 1, '2020-11-01 11:07:34', NULL);
INSERT INTO `forest_carmessage` VALUES (8, '160431739880118f830d28270456d663cac08e7710103.jpg', '豫586569', 1, '2020-11-01 11:08:07', NULL);

-- ----------------------------
-- Table structure for forest_cartype
-- ----------------------------
DROP TABLE IF EXISTS `forest_cartype`;
CREATE TABLE `forest_cartype`  (
  `typeID` int(11) NOT NULL AUTO_INCREMENT COMMENT '车位编号',
  `cartTypeName` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '车位名称',
  `carTypeDate` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '车位时间',
  `carTypeBeiZhu` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '车位备注',
  PRIMARY KEY (`typeID`) USING BTREE,
  INDEX `cartypeID`(`typeID`) USING BTREE,
  INDEX `typeID`(`typeID`) USING BTREE,
  INDEX `typeID_2`(`typeID`) USING BTREE,
  INDEX `typeID_3`(`typeID`) USING BTREE,
  INDEX `typeID_4`(`typeID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of forest_cartype
-- ----------------------------
INSERT INTO `forest_cartype` VALUES (1, '1-1', '2020-10-24 16:25:04', NULL);
INSERT INTO `forest_cartype` VALUES (2, '1-2', '2020-10-24 16:25:05', NULL);
INSERT INTO `forest_cartype` VALUES (3, '1-3', '2020-10-24 16:25:34', NULL);
INSERT INTO `forest_cartype` VALUES (4, '10-1', '2020-10-24 16:25:45', NULL);
INSERT INTO `forest_cartype` VALUES (5, '10-2', '2020-10-25 16:25:04', NULL);
INSERT INTO `forest_cartype` VALUES (6, '10-3', '2020-10-25 16:25:04', NULL);

-- ----------------------------
-- Table structure for forest_complaint
-- ----------------------------
DROP TABLE IF EXISTS `forest_complaint`;
CREATE TABLE `forest_complaint`  (
  `comID` int(11) NOT NULL AUTO_INCREMENT COMMENT '投诉ID',
  `yeZhuID` int(11) NULL DEFAULT NULL COMMENT '业主ID',
  `comTitle` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '投诉标题',
  `comContent` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '投诉内容',
  `comMatter` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '投诉类别',
  `comState` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '处理状态\r\n(1 未处理\r\n2 正在处理\r\n3 处理完成)',
  `comDate` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '投诉时间',
  PRIMARY KEY (`comID`) USING BTREE,
  INDEX `FOREST_Complaint_id1`(`yeZhuID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of forest_complaint
-- ----------------------------
INSERT INTO `forest_complaint` VALUES (1, 47, 'wanghuan1', '不行  这里面的东西好破', '物业人员', '3', '2020-11-11 13:54:58');
INSERT INTO `forest_complaint` VALUES (2, 47, '哒哒哒', '不行  这里面的东西好破', '物业服务', '2', '2020-11-11 13:55:08');

-- ----------------------------
-- Table structure for forest_currententry
-- ----------------------------
DROP TABLE IF EXISTS `forest_currententry`;
CREATE TABLE `forest_currententry`  (
  `currentEntryID` int(11) NOT NULL AUTO_INCREMENT COMMENT '日常记录ID',
  `currentEntryName` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '日常名称（姓名）',
  `xtYongHuID` int(11) NULL DEFAULT NULL COMMENT '物业ID',
  `yeZhuID` int(11) NULL DEFAULT NULL COMMENT '业主ID',
  `styleID` int(11) NULL DEFAULT NULL COMMENT '类型ID',
  `currentEntryBeiZhu` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`currentEntryID`) USING BTREE,
  INDEX `FOREST_currentry_id1`(`xtYongHuID`) USING BTREE,
  INDEX `FOREST_currentry_id2`(`yeZhuID`) USING BTREE,
  INDEX `FOREST_currentry_id3`(`styleID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of forest_currententry
-- ----------------------------
INSERT INTO `forest_currententry` VALUES (1, 'wanghuan1', 2, 1, 1, NULL);
INSERT INTO `forest_currententry` VALUES (3, 'wanghuan1', 1, 2, 1, NULL);
INSERT INTO `forest_currententry` VALUES (4, 'wanghuan1', 1, 1, 2, NULL);
INSERT INTO `forest_currententry` VALUES (5, 'wanghuan1', 2, 46, 3, NULL);
INSERT INTO `forest_currententry` VALUES (6, 'wanghuan1', 2, 47, 1, NULL);
INSERT INTO `forest_currententry` VALUES (7, 'wanghuan1', 2, 48, 1, NULL);
INSERT INTO `forest_currententry` VALUES (8, 'wanghuan1', 2, 49, 1, NULL);
INSERT INTO `forest_currententry` VALUES (10, 'wanghuan1', 2, 1, 3, NULL);
INSERT INTO `forest_currententry` VALUES (11, 'wanghuan1', 2, 8, 1, NULL);
INSERT INTO `forest_currententry` VALUES (12, 'wanghuan1', 2, 8, 1, NULL);
INSERT INTO `forest_currententry` VALUES (13, 'wanghuan1', 2, 10, 3, NULL);
INSERT INTO `forest_currententry` VALUES (14, 'wanghuan1', 2, 10, 3, NULL);

-- ----------------------------
-- Table structure for forest_currententrystyle
-- ----------------------------
DROP TABLE IF EXISTS `forest_currententrystyle`;
CREATE TABLE `forest_currententrystyle`  (
  `styleID` int(11) NOT NULL AUTO_INCREMENT COMMENT '类型ID',
  `styleName` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '操作类型',
  `styleBeiZhu` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`styleID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of forest_currententrystyle
-- ----------------------------
INSERT INTO `forest_currententrystyle` VALUES (1, '新增信息', NULL);
INSERT INTO `forest_currententrystyle` VALUES (2, '修改信息', NULL);
INSERT INTO `forest_currententrystyle` VALUES (3, '删除信息', NULL);
INSERT INTO `forest_currententrystyle` VALUES (4, '查询信息', NULL);

-- ----------------------------
-- Table structure for forest_ordermanage
-- ----------------------------
DROP TABLE IF EXISTS `forest_ordermanage`;
CREATE TABLE `forest_ordermanage`  (
  `orderID` int(11) NOT NULL AUTO_INCREMENT COMMENT '收费单ID',
  `projectID` int(11) NULL DEFAULT NULL COMMENT '收费项ID',
  `orderState` int(255) NULL DEFAULT NULL COMMENT '	收费状态（\r\n1、已收费\r\n2、未收费\r\n）',
  `orderPrice` decimal(10, 2) NULL DEFAULT NULL COMMENT '收费总价 ',
  `orderYear` int(11) NULL DEFAULT NULL COMMENT '收费年份',
  `orderMonth` int(11) NULL DEFAULT NULL COMMENT '收费月份',
  `orderDate` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '订单时间',
  `yeZhuID` int(11) NULL DEFAULT NULL COMMENT '业主ID',
  PRIMARY KEY (`orderID`) USING BTREE,
  INDEX `forest_ordermessage01`(`projectID`) USING BTREE,
  INDEX `forest_ordermessage02`(`yeZhuID`) USING BTREE,
  CONSTRAINT `forest_ordermessage01` FOREIGN KEY (`projectID`) REFERENCES `forest_projectmanage` (`projectid`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `forest_ordermessage02` FOREIGN KEY (`yeZhuID`) REFERENCES `forest_yezhumessage` (`yezhuid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 29 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of forest_ordermanage
-- ----------------------------
INSERT INTO `forest_ordermanage` VALUES (1, 1, 1, 100.00, 2020, 10, '2020-10-1', 10);
INSERT INTO `forest_ordermanage` VALUES (2, 2, 1, 200.00, 2020, 10, '2020-10-1', 7);
INSERT INTO `forest_ordermanage` VALUES (3, 1, 1, 300.00, 2020, 11, '2020-11-1', 10);

-- ----------------------------
-- Table structure for forest_projectmanage
-- ----------------------------
DROP TABLE IF EXISTS `forest_projectmanage`;
CREATE TABLE `forest_projectmanage`  (
  `projectID` int(11) NOT NULL AUTO_INCREMENT COMMENT '收费项ID',
  `projectName` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '收费项名称',
  `projectPrice` decimal(10, 2) NULL DEFAULT NULL COMMENT '单价',
  `projectDate` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '收费时间',
  PRIMARY KEY (`projectID`) USING BTREE,
  INDEX `ProjectID`(`projectID`) USING BTREE,
  INDEX `ProjectID_2`(`projectID`) USING BTREE,
  INDEX `ProjectID_3`(`projectID`) USING BTREE,
  INDEX `ProjectID_4`(`projectID`) USING BTREE,
  INDEX `ProjectID_5`(`projectID`) USING BTREE,
  INDEX `ProjectID_6`(`projectID`) USING BTREE,
  INDEX `projectID_7`(`projectID`) USING BTREE,
  INDEX `projectID_8`(`projectID`) USING BTREE,
  INDEX `projectID_9`(`projectID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of forest_projectmanage
-- ----------------------------
INSERT INTO `forest_projectmanage` VALUES (1, '水费', 0.50, '2020-09-10');
INSERT INTO `forest_projectmanage` VALUES (2, '电费', 0.60, '2020-09-20');
INSERT INTO `forest_projectmanage` VALUES (3, '服务费', 1.20, '2020-09-30');

-- ----------------------------
-- Table structure for forest_repairrequest
-- ----------------------------
DROP TABLE IF EXISTS `forest_repairrequest`;
CREATE TABLE `forest_repairrequest`  (
  `repairID` int(11) NOT NULL AUTO_INCREMENT COMMENT '维修ID',
  `repairImage` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '维修图片',
  `yeZhuID` int(11) NULL DEFAULT NULL COMMENT '业主ID',
  `shenQingShiJian` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '申请时间',
  `repairMessage` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '维修项目',
  `repairState` int(11) NULL DEFAULT NULL COMMENT '维修状态\r\n(1 未处理\r\n2 正在处理\r\n3 处理完成)',
  `repairDetail` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '维修详情',
  PRIMARY KEY (`repairID`) USING BTREE,
  INDEX `FOREST_RepairRequest_id1`(`yeZhuID`) USING BTREE,
  CONSTRAINT `FOREST_RepairRequest_id1` FOREIGN KEY (`yeZhuID`) REFERENCES `forest_yezhumessage` (`yezhuid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of forest_repairrequest
-- ----------------------------
INSERT INTO `forest_repairrequest` VALUES (1, '2d35864381a28e2baa57b962c47ae7cc.jpeg', 10, '2020-11-05 15:55:07', '空调维修', 1, '不知道');
INSERT INTO `forest_repairrequest` VALUES (2, 'b71ea2db6e8cae60e9bdbf0ad50bc92b.jpg', 7, '2020-10-24 19:15:52', '空调维修', 2, '漏电');
INSERT INTO `forest_repairrequest` VALUES (3, '17a7b61543a51948b9aa74707d1b29c0.jpg', 9, '2020-10-25 19:15:52', '空调维修', 3, '漏水地方就是附件打开发射点发大水发顺丰快递');
INSERT INTO `forest_repairrequest` VALUES (15, '160493902318017a7b61543a51948b9aa74707d1b29c0.jpg', 47, '2020-11-10 00:23:45', '空题古筝', 1, '时间太久，造成空调失去动力，缺乏氟利昂');

-- ----------------------------
-- Table structure for forest_roommessage
-- ----------------------------
DROP TABLE IF EXISTS `forest_roommessage`;
CREATE TABLE `forest_roommessage`  (
  `floorID` int(11) NOT NULL AUTO_INCREMENT COMMENT '楼盘ID',
  `floorName` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '楼盘名称',
  `floorDate` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '楼盘时间',
  `floorBeiZhu` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`floorID`) USING BTREE,
  INDEX `RoomID`(`floorID`) USING BTREE,
  INDEX `RoomID_2`(`floorID`) USING BTREE,
  INDEX `RoomID_3`(`floorID`) USING BTREE,
  INDEX `RoomID_4`(`floorID`) USING BTREE,
  INDEX `RoomID_5`(`floorID`) USING BTREE,
  INDEX `floorID`(`floorID`) USING BTREE,
  INDEX `floorID_2`(`floorID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of forest_roommessage
-- ----------------------------
INSERT INTO `forest_roommessage` VALUES (1, '1号楼', '2020-10-24 16:36:15', NULL);
INSERT INTO `forest_roommessage` VALUES (2, '2号楼', '2020-10-24 16:45:15', NULL);
INSERT INTO `forest_roommessage` VALUES (3, '3号楼', '2020-10-24 16:55:15', NULL);
INSERT INTO `forest_roommessage` VALUES (4, '4号楼', '2020-10-24 16:35:15', NULL);
INSERT INTO `forest_roommessage` VALUES (5, '5号楼', '2020-11-10 23:07:10', '1');
INSERT INTO `forest_roommessage` VALUES (6, '6号楼', '2020-11-10 23:08:56', '1');
INSERT INTO `forest_roommessage` VALUES (9, '7号楼', '2020-11-10 23:27:57', '1');

-- ----------------------------
-- Table structure for forest_roomname
-- ----------------------------
DROP TABLE IF EXISTS `forest_roomname`;
CREATE TABLE `forest_roomname`  (
  `roomID` int(11) NOT NULL AUTO_INCREMENT,
  `roomName` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `roomDate` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `roomBeiZhu` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`roomID`) USING BTREE,
  INDEX `roomID`(`roomID`) USING BTREE,
  INDEX `roomID_2`(`roomID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of forest_roomname
-- ----------------------------
INSERT INTO `forest_roomname` VALUES (1, '101', '2020-10-24 16:36:15', NULL);
INSERT INTO `forest_roomname` VALUES (2, '102', '2020-10-24 16:45:15', NULL);
INSERT INTO `forest_roomname` VALUES (3, '103', '2020-10-24 16:55:15', NULL);
INSERT INTO `forest_roomname` VALUES (4, '104', '2020-10-24 16:35:15', NULL);
INSERT INTO `forest_roomname` VALUES (7, '105', '2020-11-10 23:28:11', '1');

-- ----------------------------
-- Table structure for forest_roomnamemessage
-- ----------------------------
DROP TABLE IF EXISTS `forest_roomnamemessage`;
CREATE TABLE `forest_roomnamemessage`  (
  `roomNameID` int(11) NOT NULL AUTO_INCREMENT COMMENT '关联ID',
  `yeZhuID` int(11) NULL DEFAULT NULL COMMENT '业主ID',
  `roomID` int(11) NULL DEFAULT NULL COMMENT '房间ID',
  `floorID` int(11) NULL DEFAULT NULL COMMENT '楼盘ID',
  `roomNameDate` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '关联时间',
  `roomState` int(11) NULL DEFAULT NULL COMMENT '房间状态（\r\n0  未入住\r\n1  已入住）',
  PRIMARY KEY (`roomNameID`) USING BTREE,
  INDEX `roomNameID`(`roomNameID`) USING BTREE,
  INDEX `forest_roomNameID2`(`roomID`) USING BTREE,
  INDEX `forest_roomNameID3`(`floorID`) USING BTREE,
  INDEX `forest_roomNameID1`(`yeZhuID`) USING BTREE,
  INDEX `roomNameID_2`(`roomNameID`) USING BTREE,
  INDEX `roomNameID_3`(`roomNameID`) USING BTREE,
  INDEX `roomNameID_4`(`roomNameID`) USING BTREE,
  CONSTRAINT `forest_roomNameID1` FOREIGN KEY (`yeZhuID`) REFERENCES `forest_yezhumessage` (`yezhuid`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `forest_roomNameID2` FOREIGN KEY (`roomID`) REFERENCES `forest_roomname` (`roomid`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `forest_roomNameID3` FOREIGN KEY (`floorID`) REFERENCES `forest_roommessage` (`floorid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of forest_roomnamemessage
-- ----------------------------
INSERT INTO `forest_roomnamemessage` VALUES (2, 1, 1, 2, '2020-10-24 16:46:15', 2);
INSERT INTO `forest_roomnamemessage` VALUES (3, 1, 2, 2, '2020-10-24 17:46:15', 2);
INSERT INTO `forest_roomnamemessage` VALUES (4, 40, 4, 4, '2020-10-25 14:27:38', 1);
INSERT INTO `forest_roomnamemessage` VALUES (5, 8, 4, 3, '2020-10-25 14:27:45', 1);
INSERT INTO `forest_roomnamemessage` VALUES (6, 9, 4, 2, '2020-10-25 14:32:10', 2);
INSERT INTO `forest_roomnamemessage` VALUES (7, 8, 1, 3, '2020-10-25 14:36:39', 2);
INSERT INTO `forest_roomnamemessage` VALUES (8, 8, 2, 3, '2020-10-25 15:55:17', 2);

-- ----------------------------
-- Table structure for forest_travelrecords
-- ----------------------------
DROP TABLE IF EXISTS `forest_travelrecords`;
CREATE TABLE `forest_travelrecords`  (
  `travelRecoID` int(11) NOT NULL AUTO_INCREMENT COMMENT '出行记录ID',
  `travelRecoPlates` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '车牌照',
  `travelRecoName` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '车主姓名',
  `travelRecoSex` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '车主性别',
  `travelRecoPhone` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '车主电话',
  `travelRecoStates` int(11) NULL DEFAULT NULL COMMENT '出行状态(\r\n0   出去\r\n1  回来)',
  `travelRecoDate` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '出入时间',
  `xtYongHuID` int(11) NULL DEFAULT NULL COMMENT '物业ID',
  `carBeiZhu` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`travelRecoID`) USING BTREE,
  INDEX `FOREST_TravelRecords_id`(`xtYongHuID`) USING BTREE,
  CONSTRAINT `FOREST_TravelRecords_id` FOREIGN KEY (`xtYongHuID`) REFERENCES `forest_xitongyonghu` (`xtYongHuID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 23 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of forest_travelrecords
-- ----------------------------
INSERT INTO `forest_travelrecords` VALUES (2, '豫S556625', '王欣欣', '男', '15737626628', 2, '2020-11-01 17:04:17', 2, NULL);
INSERT INTO `forest_travelrecords` VALUES (3, '豫A23333', '王简介', '男', '15737626628', 1, '2020-11-01 17:07:36', 2, NULL);
INSERT INTO `forest_travelrecords` VALUES (4, '豫A25555', '刘丽', '女', '15737626628', 1, '2020-11-01 17:08:03', 2, NULL);
INSERT INTO `forest_travelrecords` VALUES (5, '豫A253333', '刘丽', '男', '15737626624', 1, '2020-11-01 17:08:22', 2, NULL);
INSERT INTO `forest_travelrecords` VALUES (6, '豫A253564', '李宽', '女', '15737625566', 1, '2020-11-01 17:08:42', 2, NULL);
INSERT INTO `forest_travelrecords` VALUES (7, '豫A253555', '李海', '男', '15737625567', 1, '2020-11-01 17:08:58', 2, NULL);
INSERT INTO `forest_travelrecords` VALUES (8, '豫A253558', '张旭', '男', '15737625526', 1, '2020-11-01 17:09:14', 2, NULL);
INSERT INTO `forest_travelrecords` VALUES (9, '豫A253557', '张旭', '女', '15737625529', 1, '2020-11-01 17:09:26', 2, NULL);
INSERT INTO `forest_travelrecords` VALUES (10, '豫A253558', '李兴', '男', '15737625865', 1, '2020-11-01 17:09:55', 2, NULL);
INSERT INTO `forest_travelrecords` VALUES (11, '豫B253586', '李兴', '女', '15737625865', 1, '2020-11-01 17:10:13', 2, NULL);
INSERT INTO `forest_travelrecords` VALUES (12, '豫B253587', '李兴', '男', '15737625889', 1, '2020-11-01 17:10:24', 2, NULL);
INSERT INTO `forest_travelrecords` VALUES (13, '豫C253587', '李兴', '男', '13737625889', 1, '2020-11-01 17:10:42', 2, NULL);
INSERT INTO `forest_travelrecords` VALUES (14, '豫C253588', '赵霞', '女', '13737625858', 1, '2020-11-01 17:11:15', 2, NULL);
INSERT INTO `forest_travelrecords` VALUES (15, '豫C253588', '赵霞', '女', '13737625858', 1, '2020-11-01 17:11:23', 2, NULL);
INSERT INTO `forest_travelrecords` VALUES (16, '豫C253588', '赵霞', '女', '13737625858', 1, '2020-11-01 17:11:27', 2, NULL);
INSERT INTO `forest_travelrecords` VALUES (17, '豫C253588', '赵霞', '女', '13737625858', 1, '2020-11-01 17:11:31', 2, NULL);
INSERT INTO `forest_travelrecords` VALUES (18, '豫C253588', '赵霞', '女', '13737625858', 1, '2020-11-01 17:11:35', 2, NULL);
INSERT INTO `forest_travelrecords` VALUES (19, '豫C253588', '赵霞', '女', '13737625858', 1, '2020-11-01 17:11:39', 2, NULL);
INSERT INTO `forest_travelrecords` VALUES (20, '豫C253588', '赵霞', '女', '13737625858', 2, '2020-11-01 17:12:52', 2, NULL);
INSERT INTO `forest_travelrecords` VALUES (21, '豫C253585', '赵克尔', '男', '13738525858', 1, '2020-11-01 17:12:06', 2, NULL);
INSERT INTO `forest_travelrecords` VALUES (22, '豫C253585', '赵克尔', '男', '13738525858', 1, '2020-11-01 17:12:10', 2, NULL);

-- ----------------------------
-- Table structure for forest_usercost
-- ----------------------------
DROP TABLE IF EXISTS `forest_usercost`;
CREATE TABLE `forest_usercost`  (
  `userCostId` int(11) NOT NULL AUTO_INCREMENT,
  `userCostName` int(11) NULL DEFAULT NULL,
  `userCostYear` int(11) NULL DEFAULT NULL,
  `userCostMonth` int(11) NULL DEFAULT NULL,
  `yeZhuID` int(11) NULL DEFAULT NULL,
  `projectID` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`userCostId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of forest_usercost
-- ----------------------------
INSERT INTO `forest_usercost` VALUES (1, 624, 2020, 11, 47, 1);
INSERT INTO `forest_usercost` VALUES (2, 217, 2020, 11, 47, 2);
INSERT INTO `forest_usercost` VALUES (3, 776, 2020, 11, 47, 3);

-- ----------------------------
-- Table structure for forest_xitongyonghu
-- ----------------------------
DROP TABLE IF EXISTS `forest_xitongyonghu`;
CREATE TABLE `forest_xitongyonghu`  (
  `xtYongHuID` int(11) NOT NULL AUTO_INCREMENT COMMENT '系统用户ID',
  `xtYongUserName` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '登陆名',
  `xtYongMiMa` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '用户密码',
  `xtYongName` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '用户姓名',
  `xtYongHuSFZ` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '身份证号',
  `xtYongHuSex` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '性别',
  `xtYongHuEmail` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '电子邮件',
  `xtYongHuPhone` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '手机号',
  `xtYongHuFlower` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '楼层',
  `xtYongHuState` int(1) UNSIGNED ZEROFILL NULL DEFAULT 0 COMMENT '审核状态',
  `xtYongHuJiBie` int(11) NULL DEFAULT NULL COMMENT '用户级别\r\n(2-代表管理层\r\n1代表业务人员）',
  `xtDate` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '登记时间',
  PRIMARY KEY (`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_2`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_3`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_4`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_5`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_6`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_7`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_8`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_9`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_10`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_11`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_12`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_13`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_14`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_15`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_16`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_17`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_18`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_19`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_20`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_21`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_22`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_23`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_24`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_25`(`xtYongHuID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of forest_xitongyonghu
-- ----------------------------
INSERT INTO `forest_xitongyonghu` VALUES (1, 'root', '123456789', '王**', '411526199909014514', '男', '508377135@qq.com', '15737626626', 0, 1, 3, '2020-10-9');
INSERT INTO `forest_xitongyonghu` VALUES (2, 'wanghuan1', '123123', '王欢', '411526199908014518', '男', '508377132@qq.com', '15737626627', 0, 1, 2, '2020-11-10 22:31:31');
INSERT INTO `forest_xitongyonghu` VALUES (3, 'wanghuan2', '123123', '王虎', '411526199908014589', '女', '508377152@qq.com', '15737626628', 0, 1, 1, '2020-11-10 22:34:19');
INSERT INTO `forest_xitongyonghu` VALUES (4, '呵呵', '1245678', '王好', '411526199908015263', '女', '508378132@qq.com', '15737626628', 0, 2, 1, '2020-11-10 19:18:25');

-- ----------------------------
-- Table structure for forest_yezhumessage
-- ----------------------------
DROP TABLE IF EXISTS `forest_yezhumessage`;
CREATE TABLE `forest_yezhumessage`  (
  `yeZhuID` int(11) NOT NULL AUTO_INCREMENT COMMENT '业主ID',
  `yeZhuName` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '业主姓名',
  `yeZhuSex` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '性别',
  `yeZhuPhone` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '电话',
  `yeZhuSFZ` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '身份证',
  `yeZhuEmail` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '电子邮件',
  `yeZhuPassword` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '密码',
  `yeZhuTime` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '登记时间',
  `yeZhuUserName` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '登录名',
  PRIMARY KEY (`yeZhuID`) USING BTREE,
  INDEX `YeZhuID`(`yeZhuID`) USING BTREE,
  INDEX `YeZhuID_2`(`yeZhuID`) USING BTREE,
  INDEX `YeZhuID_3`(`yeZhuID`) USING BTREE,
  INDEX `YeZhuID_4`(`yeZhuID`) USING BTREE,
  INDEX `YeZhuID_5`(`yeZhuID`) USING BTREE,
  INDEX `YeZhuID_6`(`yeZhuID`) USING BTREE,
  INDEX `YeZhuID_7`(`yeZhuID`) USING BTREE,
  INDEX `yeZhuID_8`(`yeZhuID`) USING BTREE,
  INDEX `yeZhuID_9`(`yeZhuID`) USING BTREE,
  INDEX `yeZhuID_10`(`yeZhuID`) USING BTREE,
  INDEX `yeZhuID_11`(`yeZhuID`) USING BTREE,
  INDEX `yeZhuID_12`(`yeZhuID`) USING BTREE,
  INDEX `yeZhuID_13`(`yeZhuID`) USING BTREE,
  INDEX `yeZhuID_14`(`yeZhuID`) USING BTREE,
  INDEX `yeZhuID_15`(`yeZhuID`) USING BTREE,
  INDEX `yeZhuID_16`(`yeZhuID`) USING BTREE,
  INDEX `yeZhuID_17`(`yeZhuID`) USING BTREE,
  INDEX `yeZhuID_18`(`yeZhuID`) USING BTREE,
  INDEX `yeZhuID_19`(`yeZhuID`) USING BTREE,
  INDEX `yeZhuID_20`(`yeZhuID`) USING BTREE,
  INDEX `yeZhuID_21`(`yeZhuID`) USING BTREE,
  INDEX `yeZhuID_22`(`yeZhuID`) USING BTREE,
  INDEX `yeZhuID_23`(`yeZhuID`) USING BTREE,
  INDEX `yeZhuID_24`(`yeZhuID`) USING BTREE,
  INDEX `yeZhuID_25`(`yeZhuID`) USING BTREE,
  INDEX `yeZhuID_26`(`yeZhuID`) USING BTREE,
  INDEX `yeZhuID_27`(`yeZhuID`) USING BTREE,
  INDEX `yeZhuID_28`(`yeZhuID`) USING BTREE,
  INDEX `yeZhuID_29`(`yeZhuID`) USING BTREE,
  INDEX `yeZhuID_30`(`yeZhuID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 50 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of forest_yezhumessage
-- ----------------------------
INSERT INTO `forest_yezhumessage` VALUES (1, '王欢', '男', '15737626628', '411526199908015417', '50231425@qq.com', NULL, '2020-10-24 16:35:15', NULL);
INSERT INTO `forest_yezhumessage` VALUES (5, '王喜', '女', '18956235623', '411527200009054519', '1111111@qq.com', NULL, '2020-10-24 16:34:05', NULL);
INSERT INTO `forest_yezhumessage` VALUES (6, '王哈', '男', '15705673456', '411527189009094516', '2020202220@qq.com', '', '2020-10-24 16:31:42', '');
INSERT INTO `forest_yezhumessage` VALUES (7, '王哈喽', '女', '15785673456', '411527189008014516', '2020202120@qq.com', '', '2020-10-24 17:32:33', '');
INSERT INTO `forest_yezhumessage` VALUES (8, '王简介', '男', '15775673456', '411527189008024516', '3397995074@qq.com', '123123', '2020-10-24 17:32:12', 'wanghuan');
INSERT INTO `forest_yezhumessage` VALUES (9, '王哈醒', '男', '15755673456', '411527189008044516', '2025202020@qq.com', '', '2020-10-24 16:27:17', '');
INSERT INTO `forest_yezhumessage` VALUES (10, '王哈哈', '男', '15735673456', '411527189005094516', '2025620200@qq.com', '', '2020-10-24 16:25:04', '');
INSERT INTO `forest_yezhumessage` VALUES (33, '万欢', '男', '15737626628', '411527190505064516', '508377132@qq.com', NULL, '2020-10-24 16:37:34', NULL);
INSERT INTO `forest_yezhumessage` VALUES (34, '李端', '男', '15737626689', '411526177706054519', '15737626689@qq.com', NULL, '2020-10-24 19:11:36', NULL);
INSERT INTO `forest_yezhumessage` VALUES (35, '李雪', '女', '15737626678', '411526177706054520', '15737626678@qq.com', NULL, '2020-10-24 19:12:23', NULL);
INSERT INTO `forest_yezhumessage` VALUES (36, '李波', '男', '15737626675', '411526177707054187', '15737626675@qq.com', NULL, '2020-10-24 19:12:48', NULL);
INSERT INTO `forest_yezhumessage` VALUES (37, '李博', '男', '18737626622', '411526177707052211', '18737626622@qq.com', NULL, '2020-10-24 19:13:20', NULL);
INSERT INTO `forest_yezhumessage` VALUES (38, '张旭', '男', '18737626621', '411526177707052222', '18737626621@qq.com', NULL, '2020-10-24 19:13:41', NULL);
INSERT INTO `forest_yezhumessage` VALUES (39, '张天禧', '女', '18737624517', '411526177701014125', '18737624517@qq.com', NULL, '2020-10-24 20:56:27', NULL);
INSERT INTO `forest_yezhumessage` VALUES (40, '张天下', '男', '18737624514', '411526177701014122', '18737624514@qq.com', NULL, '2020-10-24 19:14:46', NULL);
INSERT INTO `forest_yezhumessage` VALUES (41, '周深', '男', '18752034514', '411526177701055203', '18752034514@qq.com', NULL, '2020-10-24 19:15:36', NULL);
INSERT INTO `forest_yezhumessage` VALUES (42, '周空', '男', '18752034512', '411526177701055202', '18752034512@qq.com', NULL, '2020-10-24 19:15:52', NULL);
INSERT INTO `forest_yezhumessage` VALUES (43, '王虎', '男', '13525686852', '413025199905024519', '13525686852@qq.com', NULL, '2020-10-24 19:20:43', NULL);
INSERT INTO `forest_yezhumessage` VALUES (45, '王新华', '男', '13465285268', '485258166605024513', '13465285268@qq.com', NULL, '2020-10-24 21:15:52', NULL);
INSERT INTO `forest_yezhumessage` VALUES (46, '李海青', '男', '15737668889', '485258166605024556', '15737668889@qq.com', NULL, '2020-10-24 21:21:25', NULL);
INSERT INTO `forest_yezhumessage` VALUES (47, '万欢', '男', '15737626628', '452152199908044516', '508377132@qq.com', '123456', '2020-10-24 22:21:17', '王欢');
INSERT INTO `forest_yezhumessage` VALUES (48, '王特特', '男', '15737626628', '452123199906054517', '508377132@qq.com', NULL, '2020-10-24 22:29:35', NULL);

SET FOREIGN_KEY_CHECKS = 1;
